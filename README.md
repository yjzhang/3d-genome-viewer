% 3D Genome Viewer
% Yue Zhang
% May 20, 2015

3D Genome Viewer
================


## Example

1. Download <http://nerettilab.com/dna3dviz/3d_genome_browser/examples/example_for_3Dviewer.zip>, and extract the contained files. There are two structure files, two bedfiles, and two files containing removed bin indices.

2. Go to <http://nerettilab.com/dna3dviz/3d_genome_browser/5VhhqYKE7mbh3kuYSBl0Q.html>.

3. Click "Show/Hide Controls". Now, in the first control panel, upload "sen_chr18.txt" to the "Upload a structure file here" button. After this, you should see a visualization of the structure in the main view.

4. In the second control panel, change the value in the "Chromosome number/name" field to "chr18".

5. Open "removed_indices_sen_chr18.txt", and copy the list of indices to the "Excluded bins" box in the second control panel.

6. Upload the file "eigenvector_sen_chr18.txt" to the "Upload a bedfile here" button.

7. Click "Update" in either control panel. You should see the colors of the structure change. If you click on the "Description" button on the upper right of the screen, you should see a color map with the min and max values.
